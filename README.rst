braintree_sage
==============

This is a basic credit card processing application, a requirement for the
interview process at Braintree Payments.

My application is called braintree_sage, and when installed will install itself
as "sage".

Installation
------------

This is a Python project, it is recommended that you create a virtualenv.
Python 2.7 and Python 3.5 are both supported, instructions below are for using
Python 3.5, on a Unix like system, please enter the directory this project in:

    .. code::

       export VENV=./env
       python3 -m venv $VENV
       $VENV/bin/pip install -e .

This will install the project in an editable mode, from there development may
happen. Will happily accept PR's for changes, please create a topic branch, and
squash any intermediate "fixup" commits before submitting for review.

Building documentation and running tests:

    .. code::

       $VENV/bin/pip install -e .[develop]
       $VENV/bin/tox

This requires that you have Python 2.7 and Python 3.5 installed, if you would
like to just test a particular version you may use the environment flag for
tox, for example to build the docs and test on Python 3.5:

    .. code::

       $VENV/bin/tox -e py35,docs

Coverage requires that both Python 2.7 and Python 3.5 are installed, and the
two cover targets have to be run before the coverage:

    .. code::

       $VENV/bin/tox -e py2-cover,py3-cover,coverage


Documentation
-------------

Documentation is available after building from Sphinx using tox in
$PATH/.tox/docs/html/index.html. This may be opened in a browser.

License
-------

This program is licensed under an ISC license.

Authors
-------

braintree_sage was built by an anonymous person ;-)
