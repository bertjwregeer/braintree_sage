Application Design
==================

There are a couple different decisions made when building ``sage``, they are
described below.

Why Python?
-----------

I chose Python because I am most familiar with it at this point in my career.
I've worked in C++ and Java but they are not my strongest languages, and at
this point I absolutely enjoy working in Python.

I currently am the maintainer for WebOb, a Python request/response library that
is used by a huge list of projects (including Pyramid/Pylons, and OpenStack).
Alongside WebOb, I am the current maintainer for Waitress and am a core
developer for the Pylons Project.

The Python community is also absolutely fantastic and I could not imagine being
a part of a community that has helped me grow and become who I am today.

Splitting of responsibility
---------------------------

``sage`` split up the `account` from the `cardholder` (user). Eventhough the
input at the moment doesn't allow for more than a single account per
cardholder, it makes sense to possibly allow for more accounts per cardholder
in the future.

Accounts keep a transaction log
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

While we could keep a simple running counter for the balance, instead I've
implemented it as a list so that we have a transaction log. This allows
verification in tests that charge/credit's against an account actually do the
right thing.

Cardholder has a list of accounts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A Cardholder has a list of accounts, transactions may occur against any of the
cards by referencing the account number of the account, or a default card will
be used.

100% test coverage
------------------

This project requires that test coverage is 100%, without it the test suite
won't finish successfully. This makes it easier to make changes in the future
in a backwards compatible way, as any changes that lead to test failures can
now easily be investigated.

The project uses ``py.test`` because it makes it simple to write parameterized
tests, as well as integrating with coverage to verify coverage.

tox for building docs/verification of tests
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tox is used because it provides a simple way to create virtual environments for
each of the different testing environments we require (specifically Python
2.7/Python 3.5 testing). We also use this for building the documentation and
making sure that all dependencies are correctly installed.
