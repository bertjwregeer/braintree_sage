Releasing
=========

To release a new version of ``braintree_sage`` the following steps have to be
taken:

1. Make sure that `setuptools_git` is installed, this is required because this
   project does not use ``MANIFEST.in`` to define what parts of the project
   should be included in the ``sdist``/``bdist``.

   Also make sure that wheel is installed, this is used to create the universal
   wheel.

   Assuming ``$VENV`` is still setup to the appropriate path, here's the
   command to run:

   .. code::

      $VENV/bin/pip install setuptools_git wheel
2. Clean up the old ``build`` directory.

   .. code::

      rm -rf build

   This is required because using ``bdist_wheel`` to create the wheel will
   happily pack up the entire build directory, this may contain old files or
   other artifacts that should not be included in the final wheel.

3. Update ``CHANGES.txt`` and increase the version number in ``setup.py``.
4. Build the ``sdist`` and ``bdist_wheel``

   .. code::

      $VENV/bin/python3 setup.py sdist bdist_wheel


   This will drop the new release in ``./dist``, for example, for version
   ``0.0.1.dev0`` the following files will be created:

   - ``wheel``: ``braintree_sage-0.0.1.dev0-py2.py3-none-any.whl``
   - ``sdist``: ``braintree_sage-0.0.1.dev0.tar.gz``

5. Build the latest version of the docs:

   .. code::

      $VENV/bin/tox -e docs
      tar -cJf dist/braintree_sage-docs.tar.xz .tox/docs/html/

6. Once the build has been verified and everything looks correct, ``git tag``
   the current release.
