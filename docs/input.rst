.. input:

Accepted Input
==============

The application takes input that looks like this, this may be passed either on
``stdin`` or passed as a path to a file.

    .. code::
       
       Add Tom 4111111111111111 $1000
       Add Lisa 5454545454545454 $3000
       Add Quincy 1234567890123456 $2000
       Charge Tom $500
       Charge Tom $800
       Charge Lisa $7
       Credit Lisa $100
       Credit Quincy $200

The commands are documented below:

``Add``:

    Adds a new ``Cardholder`` with an ``Account`` using the credit card number,
    and the last is the limit for the ``Account`` (the maximum it may be
    charged with).

``Charge``:

    Charges the ``Cardholder``'s account, and increases the balance in the
    ``Account``. If this would increase the account above the ``Account`` limit
    that was set when it was created, then the ``Charge`` will be ignored.

``Credit``:

    Credit against the ``Cardholder``'s account, and reduces the balances in
    the ``Account``. ``Account``'s may go into a negative balance.
