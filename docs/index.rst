braintree_sage documentation
============================

This is a basic credit card processing application, a requirement for the
interview process at Braintree Payments.

My application is called ``braintree_sage``, and when installed will install
itself as a script in the Python bin directory as ``sage`` when installed using
a wheel, or from source.

.. toctree::
   :maxdepth: 2

   installation
   input
   design
   releasing

API documentation
=================

.. toctree::
   :maxdepth: 1
   :glob:

   api/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

