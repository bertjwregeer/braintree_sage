.. installation:

Installation
============

There are two ways to install ``braintree_sage``. To run all tests, and build
documentation you will need to use the source installation from git. If you'd
like to use the application you can install from the wheel.

Installation (from wheel)
-------------------------

Included with this documentation is a ``.whl`` file, this may be used to
install the software using pip. It is recommended but not required that a
virtual environment is used for Python.

The recommended Python version is Python 3.5, however Python 2.7 will also
work. This installation guide assumes Python 3.5 and that it is installed as
``python3``.

    .. code::

       export VENV=~/venv_sage
       python3 -m venv $VENV
       $VENV/bin/pip install braintree_sage-<version>.whl

After this, ``braintree_sage`` may be run by calling:

    .. code::

       $VENV/bin/sage ~/path/to/input

Installation (from source)
--------------------------

This is a full Python project, it is recommended that you create a virtualenv.
Python 2.7 and Python 3.5 are both supported, instructions below are for using
Python 3.5, on a Unix like system, please enter the directory this project in:

    .. code::

       export VENV=./env
       python3 -m venv $VENV
       $VENV/bin/pip install -e .

This will install the project in an editable mode, from there development may
happen. Will happily accept PR's for changes, please create a topic branch, and
squash any intermediate "fixup" commits before submitting for review.

Building documentation and running tests:

    .. code::

       $VENV/bin/pip install -e .[develop]
       $VENV/bin/tox

This requires that you have Python 2.7 and Python 3.5 installed, if you would
like to just test a particular version you may use the environment flag for
tox, for example to build the docs and test on Python 3.5:

    .. code::

       $VENV/bin/tox -e py35,docs

Coverage requires that both Python 2.7 and Python 3.5 are installed, and the
two cover targets have to be run before the coverage:

    .. code::

       $VENV/bin/tox -e py2-cover,py3-cover,coverage
