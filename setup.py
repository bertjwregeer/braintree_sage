import os

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
try:
    with open(os.path.join(here, 'README.rst')) as f:
        README = f.read()
    with open(os.path.join(here, 'CHANGES.txt')) as f:
        CHANGES = f.read()
except IOError:
    README = CHANGES = ''

install_requires = [
    'click',
    ]

develop_extras = [
    'tox',
    ]

docs_extras = [
    'Sphinx >= 1.3.1',
    ]

testing_extras = [
    'pytest',
    'coverage',
    'pytest-cov',
    ]

setup(
    name='braintree_sage',
    version='0.1.0',
    description="Basic Credit Card Processing",
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Other Audience",
        "License :: OSI Approved :: ISC License (ISCL)",
        "Topic :: Office/Business :: Financial",
        "Topic :: Office/Business :: Financial :: Accounting",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
    keywords='credit card',
    author='xistence',
    author_email='xistence@0x58.com',
    license='ISC',
    packages=['braintree_sage'],
    zip_safe=True,
    install_requires=install_requires,
    extras_require={
        'develop': develop_extras,
        'docs': docs_extras,
        'testing': testing_extras,
        },
    entry_points="""\
        [console_scripts]
        sage = braintree_sage.__main__:main
    """
)
