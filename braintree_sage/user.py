from braintree_sage.account import Account

class Cardholder(object):
    """Cardholder can have multiple accounts

    The cardholder is the one creating the transactions.

    :cvar name: The name of the cardholder
    :vartype name: str
    """

    default_card_index = 0
    account_class = Account # testing

    def __init__(self, name):
        self.name = name
        self.accounts = []

    def create_account(self, ccn, limit, starting=0):
        """ Create a new account for the Cardholder

        :cvar ccn: The credit card number for the new account
        :vartype ccn: int or str

        :cvar limit: The limit of the new account
        :vartype limit: int

        :cvar starting: The starting value of the account, default is 0
        :vartype starting: int
        """
        self.accounts.append(self.account_class(ccn, limit, starting=starting))

    def charge(self, amount, account_number=None):
        self.exec_cmd('charge', amount, account_number=account_number)

    def credit(self, amount, account_number=None):
        self.exec_cmd('credit', amount, account_number=account_number)

    def exec_cmd(self, cmd, amount, account_number=None):
        """ Execute a command against an Account

        :cvar cmd: The command to execute against an account, valid ones
            include: (charge, credit)
        :vartype cmd: str

        :cvar amount: The amount to use when executing the command
        :vartype amount: int

        :cvar account_number: The account number to execute the command
            against. The default is None, in which cacse the default_card_index
            will be used to select the default card.
        :vartype account_number: int or str

        """
        if not self.accounts:
            raise RuntimeError('Cardholder has no accounts.')

        exec_account = None

        if account_number is None:
            exec_account = self.accounts[0]
        else:
            for account in self.accounts:
                if account.ccn == account_number:
                    exec_account = account

            if exec_account is None:
                raise RuntimeError('Account number does not exist.')

        if cmd == 'charge':
            exec_account.charge(amount)

        if cmd == 'credit':
            exec_account.credit(amount)

    @property
    def balance(self):
        balance = 0
        valid = False
        for account in self.accounts:
            if account.valid:
                balance += account.balance
                valid = True

        if valid:
            return balance
        else:
            return None
