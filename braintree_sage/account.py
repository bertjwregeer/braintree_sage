from braintree_sage import luhn

class Account(object):
    """Holds account state

    This holds all of the account state, including dollar amounts and
    information about the card number, limit, and including whether it is valid
    or not.

    :cvar ccn: Credit Card number for the account
    :vartype ccn: int or str

    :cvar limit: The limit on the credit card
    :vartype limit: int

    :cvar starting: The starting balance of the account, default is 0
    :vartype starting: int
    """

    def __init__(self, ccn, limit, starting=0):
        self.ccn = str(ccn)
        self.limit = int(limit)
        self._valid = luhn.is_luhn_valid(self.ccn)
        self.transactions = [
            starting,
        ]

    def charge(self, amount):
        """ Charge the account

        If the account is considered valid, and the charge amount won't
        increase the account past the limit then the charge is applied to the
        account.

        :cvar amount: The amount to charge the account by
        :vartype amount: int

        """
        amount = int(amount)
        if self.valid and (self.amount + amount) <= self.limit:
            self.transactions.append(amount)

    def credit(self, amount):
        """ Credit the account

        If the account is considered valid, the credit is applied to the
        account's transaction list.

        :cvar amount: The amount to credit from the account
        :vartype amount: int

        """
        amount = int(amount)

        if self.valid:
            self.transactions.append(-amount)

    @property
    def balance(self):
        """ Return the balance of the account """
        if self.valid:
            return sum(self.transactions)
        else:
            return None

    @property
    def amount(self):
        """ Return the balance of the account (deprecated) """
        return self.balance

    @property
    def valid(self):
        """ Returns whether the account is currently valid

        This is based upon the Credit Card Number passing the Luhn algorithm
        check.
        """
        return self._valid
