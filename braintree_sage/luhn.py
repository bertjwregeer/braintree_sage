# Shamelessly lifted from https://en.wikipedia.org/wiki/Luhn_algorithm

def digits_of(number):
    return list(map(int, str(number)))

def luhn_checksum(card_number):
    """ Returns the luhn algorithm checksum """
    digits = digits_of(card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    total = sum(odd_digits)
    for digit in even_digits:
        total += sum(digits_of(2 * digit))
    return total % 10

def is_luhn_valid(card_number):
    """ Checks if a given card number is valid according to the Luhn algorithm
    """
    return luhn_checksum(card_number) == 0
