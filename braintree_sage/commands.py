def parse(lines):
    for line in lines:
        cmd, args = line.split(' ', 1)
        args = args.split(' ')

        yield (cmd.strip(), [arg.strip() for arg in args])
