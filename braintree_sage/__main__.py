import click

from braintree_sage import commands
from braintree_sage.user import Cardholder

@click.command()
@click.argument('input', type=click.File('rb'), default='-')
def main(input):
    def decode_input():
        for l in input:
            yield l.decode('utf-8').strip()

    parsed_commands = list(commands.parse(decode_input()))

    cardholders = []

    for (cmd, args) in parsed_commands:
        if cmd == 'Add':
            (name, ccn, limit) = args
            cardholder = Cardholder(name.strip())
            cardholder.create_account(ccn.strip(), int(limit.lstrip('$')))
            cardholders.append(cardholder)

        if cmd == 'Charge':
            (name, amount) = args
            for cardholder in cardholders:
                if cardholder.name == name.strip():
                    cardholder.charge(int(amount.lstrip('$')))
                    break
            else:
                print('Invalid Charge, cardholder "{}" does not exist.'.format(name))

        if cmd == 'Credit':
            (name, amount) = args
            for cardholder in cardholders:
                if cardholder.name == name.strip():
                    cardholder.credit(int(amount.lstrip('$')))
                    break
            else:
                print('Invalid Credit, cardholder "{}" does not exist.'.format(name))

    for cardholder in sorted(cardholders, key=lambda cholder: cholder.name.lower()):
        fmt = '{name}: {balance}'

        name = cardholder.name
        balance = cardholder.balance

        if balance is None:
            balance = 'error'
        else:
            balance = '${}'.format(balance)

        print(fmt.format(name=name, balance=balance))

if __name__ == "__main__": # pragma: nocover
    main()
