import pytest

from braintree_sage import luhn

def test_digits_of():
    assert [1, 2, 3, 4, 5] == luhn.digits_of(12345)
    assert [1, 2, 3, 4, 5] == luhn.digits_of('12345')

testcards = [
    ('349941874564755', 0),   # AMEX
    ('6011740597391775', 0),  # Discover
    ('5128355442076059', 0),  # MasterCard
    ('4532462013619318', 0),  # Visa
    ('4242424242424242', 0),  # Visa
    ('30569309025904', 0),    # Diners Club
    ('3566002020360505', 0),  # JCB
    ('123456', 1),            # Test number
    ('1234568', 2),           # Test number
]

@pytest.mark.parametrize("ccn,result", testcards)
def test_luhn_checksum(ccn, result):
    assert result == luhn.luhn_checksum(ccn)

@pytest.mark.parametrize(
    "ccn,result",
    [
        ('349941874564755', True),
        ('349941874564754', False),
    ])
def test_valid_luhn(ccn, result):
    assert luhn.is_luhn_valid(ccn) is result
