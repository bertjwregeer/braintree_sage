from click.testing import CliRunner

from braintree_sage.__main__ import main

def test_main():
    runner = CliRunner()

    result = runner.invoke(main, ['./tests/samples/provided_example.txt'])
    assert not result.exception
    assert result.output == 'Lisa: $-93\nQuincy: error\nTom: $500\n'

def test_main_invalid_op():
    runner = CliRunner()

    result = runner.invoke(main, ['./tests/samples/invalid_ops.txt'])
    assert not result.exception
    assert result.output == 'Invalid Charge, cardholder "John" does not '\
        'exist.\nInvalid Credit, cardholder "Jane" does not exist.\nLisa: '\
        '$-93\nQuincy: error\nTom: $500\n'
