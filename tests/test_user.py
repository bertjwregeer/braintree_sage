import pytest

from braintree_sage.user import Cardholder

class Cardholder_tests(object):
    def test_create_cardholder(self):
        cholder = self._makeOne('John')
        assert cholder.name == 'John'
        assert len(cholder.accounts) == 0

    def test_create_account(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)
        account = cholder.accounts[0]

        assert account.ccn == '349941874564755'
        assert account.limit == 1000
        assert account.transactions == [0]

    def test_create_account_starting(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000, starting=50)
        account = cholder.accounts[0]

        assert account.ccn == '349941874564755'
        assert account.limit == 1000
        assert account.transactions == [50]

    def test_command_charge(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000, starting=100)
        account = cholder.accounts[0]

        assert account.ccn == '349941874564755'
        assert account.limit == 1000
        assert account.transactions == [100]

        cholder.exec_cmd('charge', 50)

        assert account.transactions == [100, 50]

    def test_command_credit(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000, starting=100)
        account = cholder.accounts[0]

        assert account.ccn == '349941874564755'
        assert account.limit == 1000
        assert account.transactions == [100]

        cholder.exec_cmd('credit', 50)

        assert account.transactions == [100, -50]

    def test_command_no_account(self):
        cholder = self._makeOne('John')

        with pytest.raises(RuntimeError):
            cholder.exec_cmd('credit', 50)

    def test_create_accounts(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)
        cholder.create_account('5128355442076059', 500)

        assert len(cholder.accounts) == 2

    def test_command_specific_account(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)
        cholder.create_account('5128355442076059', 500)

        cholder.exec_cmd('credit', 50, account_number='349941874564755')
        cholder.exec_cmd('charge', 500, account_number='5128355442076059')

        account0 = cholder.accounts[0]
        account1 = cholder.accounts[1]

        assert account0.limit == 1000
        assert account1.limit == 500
        assert account0.transactions == [0, -50]
        assert account1.transactions == [0, 500]

    def test_command_invalid_account(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)

        with pytest.raises(RuntimeError):
            cholder.exec_cmd('credit', 50, account_number='invalid')

    def test_charge(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)

        cholder.charge(500)

        account = cholder.accounts[0]

        assert account.transactions == [0, 500]

    def test_credit(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)

        cholder.credit(500)

        account = cholder.accounts[0]

        assert account.transactions == [0, -500]

    def test_balance(self):
        cholder = self._makeOne('John')
        cholder.create_account('349941874564755', 1000)
        cholder.create_account('5128355442076059', 500)

        cholder.exec_cmd('credit', 50, account_number='349941874564755')
        cholder.exec_cmd('charge', 500, account_number='5128355442076059')

        account0 = cholder.accounts[0]
        account1 = cholder.accounts[1]

        assert account0.limit == 1000
        assert account1.limit == 500
        assert account0.transactions == [0, -50]
        assert account1.transactions == [0, 500]

        assert cholder.balance == 450

    def test_balance_invalid(self):
        cholder = self._makeOne('John')
        cholder.create_account('1234567', 1000)

        account = cholder.accounts[0]

        assert account.limit == 1000
        assert account.transactions == [0]

        if isinstance(account, DummyAccount):
            account.valid = False

        cholder.exec_cmd('credit', 50)

        assert cholder.balance is None


class TestCardholderUnit(Cardholder_tests):
    # These tests use a dummy class for unit testing without worrying about
    # integration testing directly.

    def _makeOne(self, name):
        cholder = Cardholder(name)
        cholder.account_class = DummyAccount

        return cholder


class TestCardholderIntegration(Cardholder_tests):
    def _makeOne(self, name):
        cholder = Cardholder(name)
        return cholder


class DummyAccount(object):
    def __init__(self, ccn, limit, starting=0):
        self.ccn = ccn
        self.limit = limit
        self.transactions = [
            starting,
        ]
        self.valid = True

    def charge(self, amount):
        if self.valid:
            self.transactions.append(amount)

    def credit(self, amount):
        if self.valid:
            self.transactions.append(-amount)

    @property
    def amount(self):
        return sum(self.transactions)

    balance = amount
