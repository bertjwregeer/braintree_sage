from braintree_sage.account import Account

def test_create_account_invalid_ccn():
    account = Account('123456', 1000)
    assert account.valid is False
    assert account.limit == 1000
    assert account.amount is None

def test_create_account_valid_ccn():
    account = Account('349941874564755', 1000)
    assert account.valid is True
    assert account.limit == 1000
    assert [0] == account.transactions
    assert 0 == account.amount

def test_account_charge_valid():
    account = Account('349941874564755', 1000)
    assert account.valid is True
    assert account.limit == 1000
    account.charge(1000)
    assert 1000 == account.amount
    assert [0, 1000] == account.transactions

def test_account_charge_invalid():
    account = Account('349941874564755', 1000)
    assert account.valid is True
    account.charge(2000)
    assert 0 == account.amount
    assert [0] == account.transactions

def test_account_credit():
    account = Account('349941874564755', 1000)
    assert account.valid is True
    account.charge(500)
    account.credit(400)
    assert 100 == account.amount
    assert [0, 500, -400] == account.transactions

def test_account_credit_negative():
    account = Account('349941874564755', 1000)
    assert account.valid is True
    account.charge(500)
    account.credit(800)
    assert -300 == account.amount
    assert [0, 500, -800] == account.transactions
