from braintree_sage import commands

def test_parse_command():
    input = [
        'Add Tom 4111111111111111 $1000'
    ]

    expected = [
        ('Add', ['Tom', '4111111111111111', '$1000'])
    ]

    assert list(commands.parse(input)) == expected

def test_parse_multiple():
    input = [
        'Add Tom 4111111111111111 $1000',
        'Charge Tom $500',
    ]

    expected = [
        ('Add', ['Tom', '4111111111111111', '$1000']),
        ('Charge', ['Tom', '$500']),
    ]

    assert list(commands.parse(input)) == expected
